package com.LocTroi.DTO;

import java.util.Date;

import org.modelmapper.ModelMapper;

public class orderDTO {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFirtName() {
		return firtName;
	}
	public void setFirtName(String firtName) {
		this.firtName = firtName;
	}
	public String getFarmerName() {
		return farmerName;
	}
	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Double getTotalMoneyFinish() {
		return totalMoneyFinish;
	}
	public void setTotalMoneyFinish(Double totalMoneyFinish) {
		this.totalMoneyFinish = totalMoneyFinish;
	}
	public String getWardName() {
		return wardName;
	}
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}
	public String getDistrictsName() {
		return districtsName;
	}
	public void setDistrictsName(String districtsName) {
		this.districtsName = districtsName;
	}
	private String orderCode;
	private Date createDate;
	private int status;
	private String firtName;
	private String farmerName;
	private String shopName;
	private Double totalMoneyFinish;
	private String wardName;
	private String districtsName;
	
	public static <T> T transferObject(Object model, Class<T> type){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(model, type);
    }
	

}
