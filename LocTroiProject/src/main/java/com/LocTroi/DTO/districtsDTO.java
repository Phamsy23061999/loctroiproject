package com.LocTroi.DTO;

import org.modelmapper.ModelMapper;

public class districtsDTO {

	private int id;
	private String name;
	
	 public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static <T> T transferObject(Object model, Class<T> type){
	        ModelMapper modelMapper = new ModelMapper();
	        return modelMapper.map(model, type);
	    }
}
