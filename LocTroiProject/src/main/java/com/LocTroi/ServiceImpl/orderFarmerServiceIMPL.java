package com.LocTroi.ServiceImpl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.LocTroi.DAO.orderFarmerDAO;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Service.orderFarmerService;

import net.minidev.json.JSONObject;

@Service
public class orderFarmerServiceIMPL implements orderFarmerService{

	@Autowired 
	orderFarmerDAO orderDAO; 
	@Override
	public JSONObject findByOrderFarmer(Date dateStart, Date dateFinish) {
		JSONObject js = new JSONObject();
		try {
			js.put("List order", orderDAO.findByOrderFarmer(dateStart, dateFinish));
			
		} catch (Exception e) {
			js.put("Error", "Có lỗi xảy Ra");
		}
		return js;
	}

}
