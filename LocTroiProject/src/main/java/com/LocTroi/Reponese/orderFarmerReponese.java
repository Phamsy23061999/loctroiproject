package com.LocTroi.Reponese;

import java.util.List;

import com.LocTroi.DTO.citiesDTO;
import com.LocTroi.DTO.farmerDTO;
import com.LocTroi.DTO.orderFarmerDTO;
import com.LocTroi.DTO.userDTO;
import com.LocTroi.DTO.wardsDTO;
import com.LocTroi.Model.orderFarmerEntity;

public class orderFarmerReponese {
	
	private List<userDTO> userdto;
	private List<farmerDTO>farmerDTOs;
	private List<wardsDTO> wardsdto;
	private List<citiesDTO> citydto;
	private List<orderFarmerDTO> orderFarmerDTO;
	public List<orderFarmerDTO> getOrderFarmerDTO() {
		return orderFarmerDTO;
	}

	public void setOrderFarmerDTO(List<orderFarmerDTO> orderFarmerDTO) {
		this.orderFarmerDTO = orderFarmerDTO;
	}

	public orderFarmerReponese(List<userDTO> userdto, List<farmerDTO> farmerDTOs, List<wardsDTO> wardsdto,
			List<citiesDTO> citydto) {
		this.userdto = userdto;
		this.farmerDTOs = farmerDTOs;
		this.wardsdto = wardsdto;
		this.citydto = citydto;
	}
	
	public orderFarmerReponese() {
		// TODO Auto-generated constructor stub
	}

	public List<userDTO> getUserdto() {
		return userdto;
	}
	public void setUserdto(List<userDTO> userdto) {
		this.userdto = userdto;
	}
	public List<farmerDTO> getFarmerDTOs() {
		return farmerDTOs;
	}
	public void setFarmerDTOs(List<farmerDTO> farmerDTOs) {
		this.farmerDTOs = farmerDTOs;
	}
	public List<wardsDTO> getWardsdto() {
		return wardsdto;
	}
	public void setWardsdto(List<wardsDTO> wardsdto) {
		this.wardsdto = wardsdto;
	}
	public List<citiesDTO> getCitydto() {
		return citydto;
	}
	public void setCitydto(List<citiesDTO> citydto) {
		this.citydto = citydto;
	}
	
		
	
}
