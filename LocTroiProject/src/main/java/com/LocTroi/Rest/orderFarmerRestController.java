package com.LocTroi.Rest;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.LocTroi.ServiceImpl.orderFarmerServiceIMPL;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/order")
public class orderFarmerRestController {
	
	@Autowired
	orderFarmerServiceIMPL orderIMPL;
	
	@GetMapping("/get/order_farmer")
	public JSONObject findByOrderFarmer(@RequestParam("dateStart")Date dateStart,@RequestParam("dateFinish") Date dateFinish) {
		return orderIMPL.findByOrderFarmer(dateStart, dateFinish);
	}

}
 