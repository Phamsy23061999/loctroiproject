package com.LocTroi.Service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.LocTroi.Model.orderFarmerEntity;

import net.minidev.json.JSONObject;

@Service
public interface orderFarmerService {
	
	JSONObject findByOrderFarmer(Date dateStart, Date dateFinish);

}
