package com.LocTroi.Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.citiesDTO;
import com.LocTroi.DTO.districtsDTO;
import com.LocTroi.DTO.farmerDTO;
import com.LocTroi.DTO.orderFarmerDTO;
import com.LocTroi.DTO.userDTO;
import com.LocTroi.DTO.wardsDTO;
import com.LocTroi.Model.OrderEntity;
import com.LocTroi.Model.citiesEntity;
import com.LocTroi.Model.districtsEntity;
import com.LocTroi.Model.farmerEntity;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Model.userEntity;
import com.LocTroi.Model.wardsEntity;



@Repository
public interface orderFarmerRepository extends JpaRepository<orderFarmerEntity, Integer> {
	
	@Query(value="SELECT od.id, od.order_code, od.create_date, od.status ,od.total_money_finish FROM order_farmer od  WHERE Date(od.create_date) >= date(?1) and date(od.create_date) <= date(?2)",nativeQuery = true)
	List<orderFarmerDTO> findByFarmerOrderCreatedate(@Param("dateStart") Date dateStart, @Param("dateFinish") Date dateFinish);
	
	@Query(value = "SELECT us.* FROM order_farmer od  LEFT JOIN user us on us.id = od.id ",nativeQuery = true)
	List<userDTO> findByOrderFarmer();
	
	@Query(value = "SELECT fm.* FROM order_farmer od LEFT JOIN farmer fm on fm.id = od.farmer_id ",nativeQuery = true)
	List<farmerDTO> findByFarmer();
	
	@Query(value = "SELECT wa.* From order_farmer od LEFT JOIN wards wa on wa.id = od.ward_id  ",nativeQuery = true)
	List< wardsDTO> findByWard();
	
	@Query(value = "SELECT di.* FROM order_farmer od LEFT JOIN districts di on di.id = od.district_id ", nativeQuery = true)
	List<districtsDTO> findByDistrict();
	
	@Query(value = "SELECT ci.* FROM order_farmer od LEFT JOIN cities ci on ci.id = od.city_id ",nativeQuery = true)
	List<citiesDTO> findByCity();
	
	
}




