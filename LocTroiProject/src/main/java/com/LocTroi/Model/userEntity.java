package com.LocTroi.Model;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.aspectj.weaver.tools.Trace;

@Entity
public class userEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "sale_man_code")
	private String saleManCode;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "avartar")
	private String avartar;
	
	@Column(name = "cities_id")
	private int citiesId;
	
	@Column(name = "country_code")
	private String countryCode;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "deviceid")
	private String deviceId;
	
	@Column(name = "district_id")
	private int districtId;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "firebase_devicetoken")
	private String firebaseDevicetoken;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "os")
	private String os;
	
	@Column(name = "otp")
	private String otp;
	
	@Column(name = "shop_name")
	private String shopName;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "update_time")
	private Time updateTime;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "ward_id")
	private int wardId;
	
	@Column(name = "vip")
	private int vip;
	
	@Column(name = "level_up_require")
	private String levelUpRequire;
	
	@Column(name = "total_buy")
	private int total_buy;
	
	@Column(name = "point")
	private int point;
	
	@Column(name = "supplier_id")
	private int supplierId;
	
	@Column(name = "view")
	private int view;
	
	@Column(name = "shop_type")
	private int shopType;
	
	@Column(name = "shop_channel")
	private int shopChannel;
	
	@Column(name = "active_push")
	private int activePush;
	
	@Column(name = "phone2")
	private String phone2;
	
	@Column(name = "bussiness_name")
	private String bussinessName;
	
	@Column(name = "bussiness_tax_code")
	private String bussinessTaxCode;
	
	@Column(name = "bussiness_address")
	private String bussinessAddress;
	
	@Column(name = "images")
	private String images;
	
	@Column(name = "lat")
	private Double lat;
	
	@Column(name = "long")
	private Double Long;
	
	@Column(name = "dms_code")
	private String dmsCode;
	
	@Column(name = "address_delivery")
	private String address_delivery;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
