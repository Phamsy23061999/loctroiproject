package com.LocTroi.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

public class farmerEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name= "farmer_name")
	private String farmerName;
	
	@Column(name= "username")
	private String userName;
	
	@Column(name= "phone")
	private String phone;
	
	@Column(name= "farmer-code")
	private String farmerCode;
	
	@Column(name= "avatar")
	private String avatar;
	
	@Column(name= "address")
	private String address;
	
	@Column(name= "ward_id")
	private int wardId;
	
	@Column(name= "district_id")
	private int districtId;
	
	@Column(name= "status")
	private int status;
	
	@Column(name= "deleted")
	private int deleted;
	
	@Column(name= "create_date")
	private int createDate;
	
	@Column(name= "update_time")
	private int updateTime;
	
	@Column(name="s2_id")
	private int s2Id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFarmerName() {
		return farmerName;
	}

	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getWardId() {
		return wardId;
	}

	public void setWardId(int wardId) {
		this.wardId = wardId;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public int getCreateDate() {
		return createDate;
	}

	public void setCreateDate(int createDate) {
		this.createDate = createDate;
	}

	public int getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(int updateTime) {
		this.updateTime = updateTime;
	}

	public int getS2Id() {
		return s2Id;
	}

	public void setS2Id(int s2Id) {
		this.s2Id = s2Id;
	}
	
	
	
	
	
	
	
	
	
	

}
