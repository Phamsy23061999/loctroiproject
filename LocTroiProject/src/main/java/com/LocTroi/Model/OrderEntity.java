package com.LocTroi.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OrderEntity {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "order_code")
	private String orderCode;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "firt_name")
	private String firtName;
	
	
	@Column(name = "farmer_name")
	private String farmerName;
	
	@Column(name = "shop_name")
	private String shopName;
	
	@Column(name = "total_money_finish")
	private Double totalMoneyFinish;
	
	@Column(name = "wardName")
	private String wardName;
	
	@Column(name = "districtsName")
	private String districtsName;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFirtName() {
		return firtName;
	}

	public void setFirtName(String firtName) {
		this.firtName = firtName;
	}

	public String getFarmerName() {
		return farmerName;
	}

	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Double getTotalMoneyFinish() {
		return totalMoneyFinish;
	}

	public void setTotalMoneyFinish(Double totalMoneyFinish) {
		this.totalMoneyFinish = totalMoneyFinish;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public String getDistrictsName() {
		return districtsName;
	}

	public void setDistrictsName(String districtsName) {
		this.districtsName = districtsName;
	}

	public String getCitiesName() {
		return citiesName;
	}

	public void setCitiesName(String citiesName) {
		this.citiesName = citiesName;
	}

	@Column(name= "citiesName")
	private String citiesName;
	
	
	
}
