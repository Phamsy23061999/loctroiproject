package com;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;


@SpringBootApplication
public class Application {
		public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
		}
		
		@InitBinder
		protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");   
		    dateFormat.setLenient(false);
		    binder.registerCustomEditor(Date.class, null,  new CustomDateEditor(dateFormat, true));
		}

}
